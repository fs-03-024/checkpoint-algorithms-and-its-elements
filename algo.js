function analyserPhrase(phrase) {
    // Initialisation des compteurs
    let longueurPhrase = 0;
    let nombreMots = 0;
    let nombreVoyelles = 0;
  
    // Parcourir chaque caractère de la phrase
    for (let i = 0; i < phrase.length; i++) {
      // Incrémenter la longueur de la phrase
      longueurPhrase++;
  
      // Vérifier si le caractère est une lettre
      if (phrase[i].match(/[a-zA-Z]/)) {
        // Vérifier si le caractère est une voyelle
        if (phrase[i].match(/[aeiouAEIOU]/)) {
          // Incrémenter le compteur de voyelles
          nombreVoyelles++;
        }
  
        // Vérifier si le caractère est un espace ou le premier caractère
        if (phrase[i] === " " || i === 0) {
          // Incrémenter le compteur de mots
          nombreMots++;
        }
      }
    }
  
    // Afficher les résultats
    console.log("Longueur de la phrase :", longueurPhrase);
    console.log("Nombre de mots :", nombreMots);
    console.log("Nombre de voyelles :", nombreVoyelles);
  }
  
  // Exemple d'utilisation
  const phrase = "Le chien aboie. La nuit est calme.";
  analyserPhrase(phrase);